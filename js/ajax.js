/**
 * Created by DJ on 6/10/2017.
 */
function openTab(entryConst) {

    var filename = "./ajax/change_tab.ajax.php";
    console.log(entryConst);

    $.ajax({
        'url': 'index.php',
        'type': 'POST',
        'data': {
            entryConst: entryConst,
            file: filename,
            AJAX: true
        },
        success : function(data) {
            console.log(data);

            window.location.reload();
        },
        error : function(request, error) {

        }
    });

}