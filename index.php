<?php

session_start();

//<editor-fold desc="Error Reporting">

// setup error reporting
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//</editor-fold>

//<editor-fold desc="Tab Constants">

const HOME_TAB = 20;
const DOGS_FOR_SALE_TAB = 21;
const OUR_MALES_TAB = 22;
const OUR_FEMALES_TAB = 23;
const CONTACT_US_TAB = 24;

//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Autoload">

// require the autoload class
require_once("../resources/php/autoload/autoload.class.php");

// Instantiate the autoloader
new Autoload(["php", '../resources/php/']);

//</editor-fold>


$runShell = TRUE;

if(isset($_REQUEST['AJAX']) && $_REQUEST['AJAX'] == TRUE) {

    $runShell = FALSE;

    if(isset($_REQUEST['file'])) {
        include($_REQUEST['file']);
        die('');
    }
}

if($runShell) {

    // init shell
    $shell = new Shell();

    // get the html from the shell with page title
    $html = $shell->getHtml("CDF Kennels");

    // echo the html
    echo $html;

}