<?php

//<editor-fold defaultstate="collapsed" desc="Autoload">

// require the autoload class
require_once("../../resources/php/autoload/autoload.class.php");

// Instantiate the autoloader
new Autoload(["../php", '../../resources/php/']);

//</editor-fold>

$doctype = "<!DOCTYPE html>";

$html = new Html_Element("html");

$body = new Html_Element("body");

// init the form
$form = new Html_Element("form");

$form->action = "upload.php";
$form->method = "post";
$form->enctype = "multipart/form-data";

// init file upload input
$fileUploadInput = new Html_Element("input");
$fileUploadInput->type = "file";
$fileUploadInput->name = "fileToUpload";
$fileUploadInput->id = "fileToUpload";

// init file submit input
$fileSubmit = new Html_Element("input");
$fileSubmit->type = "submit";
$fileSubmit->value = "Upload Image";
$fileSubmit->name = "submit";

// append inputs to form
$form->text .= "Select image to upload:";
$form->text .= $fileUploadInput;
$form->text .= $fileSubmit;

// append form to body
$body->text .= $form;

// append body to html
$html->text .= $body;

// echo
echo $doctype . $html;