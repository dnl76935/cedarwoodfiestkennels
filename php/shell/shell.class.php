<?php

class Shell extends Shell_V0 {

    // the callbacks for all of the tab constants
    protected $tabConfig = [
        HOME_TAB => "getHomeBody",
        DOGS_FOR_SALE_TAB => "getDogsForSaleBody",
        OUR_MALES_TAB => "getOurMalesBody",
        OUR_FEMALES_TAB => "getOurFemalesBody",
        CONTACT_US_TAB => "getContactUsBody",
    ];

    public function getHtml($title) {

        // init html with head tag
        $html = "<!DOCTYPE html> <html>" . $this->getHead($title);

        // init body
        $body = new Html_Element("body", ["style" => "background-color: orange ;"]);

        // standard container for the page
        // this should go under the nav bar
        $page = new Html_Element("div");
        $page->class .= " page ";

        // if the tab is set, get it
        // else, set the tab to the home tab and get it
        if(isset($_SESSION["tab"])) {
            $tab = $_SESSION["tab"];
        } else {
            $_SESSION["tab"] = HOME_TAB;
            $tab = $_SESSION["tab"];
        }

        // get the content for the selected tab and append it to the page
        $content = $this->{$this->tabConfig[$tab]}();
        $page->text .= $content;

        // get the navbar
        $nav = new Navbar();

        // append the nav and the page to the body
        $body->text .= $nav;
        $body->text .= $page;

        // append the body to the html and close it
        $html .= $body . "</html>";

        // return
        return $html;

    }

	protected function getHead($title) {
		
		$head = parent::getHead($title);
		
		// compiled and minified bootstrap CSS
		$bootCSS = new Html_Element("link");
		
		$bootCSS->rel = "stylesheet";
		$bootCSS->href = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css";
		$bootCSS->integrity = "sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u";
		$bootCSS->crossorigin = "anonymous";
		
		$head->text .= $bootCSS;
		
		// optional bootstrap theme
		$bootOpt = new Html_Element("link");
		
		$bootOpt->rel = "stylesheet";
		$bootOpt->href = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css";
		$bootOpt->integrity = "sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp";
		$bootOpt->crossorigin = "anonymous";
		
		$head->text .= $bootOpt;
		
		// compiled and minified bootstrap JS
		$bootJS = new Html_Element("script");
		
		$bootJS->src = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js";
		$bootJS->integrity = "sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa";
		$bootJS->crossorigin = "anonymous";
		
		$head->text .= $bootJS;
		
		return $head;
		
	}

	protected function getBody() {
		
		$body = new Html_Element("body");
		
		$nav = new Navbar();
		$info = new Home_Content();
		
		$body->text .= $nav->getContainer();
		$body->text .= $info->getContainer();
		
		return $body;
		
	}

	protected function getHomeBody() {

        // get the content for the home page
        $homeContent = new Home_Content();

        // return
        return $homeContent;

    }

    protected function getDogsForSaleBody() {

        return "";

    }

    protected function getOurMalesBody() {

        return "";

    }

    protected function getOurFemalesBody() {

        return "";

    }

    protected function getContactUsBody() {

        return "";

    }

	
}