<?php

class Home_Content {
	
	public function __construct() {
		
	}

	public function __toString() {
        return $this->getContainer()->__toString();
    }

    public function getContainer() : Html_Element {
		
		$container = new Html_Element("div", ["class" => "container"]);
		
		$homeContainer = new Html_Element("div", ["id" => "home", "class" => "tabContent"]);
		
		$content = new Html_Element();
		
		$content->text .= "Hi mom";
		
		$homeContainer->text .= $content;
		
		$container->text .= $homeContainer;
		
		return $container;
		
	}
	
}